﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SimpleTCP;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Sockets;
using System.Collections.Concurrent;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using Newtonsoft.Json;
using System.Windows.Threading;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using PortabelloRoni.BL;
using PortabelloRoni.Global;

namespace PortabelloRoni.DAL
{

    public class TCPIP
    {

        public SimpleTcpClient TcpClient = new SimpleTcpClient();
        public SimpleTcpClient TcpClientCamera = new SimpleTcpClient();



        public TCPIP()
        {
            this.TcpClient = new SimpleTcpClient();
            this.TcpClientCamera = new SimpleTcpClient();
        }

      

        public bool Connect(string Ip, string Port)
        {

            try
            {

                TcpClient.Connect(Ip, int.Parse(Port));
                TcpClient.WriteLineAndGetReply(StaticNotiication.Connected, TimeSpan.FromSeconds(3));
            }
            catch
            {
               
                return false;

            }
            return true;
        }

       

        public bool ConnectCamera(string Ip, string Port)
        {

            try
            {
                               
                TcpClientCamera.Connect(Ip, int.Parse(Port));
                TcpClientCamera.WriteLineAndGetReply(StaticNotiication.Connected, TimeSpan.FromSeconds(3));
                var thread = new Thread(LoadCam);
                thread.Start();
            }
            catch
            {
                return false;

            }
            return true;
        }

        public bool Send(string MessageString)
        {

            try
            {

                TcpClient.WriteLine(MessageString);
                return true;

            }

            catch
            {
                return false;
                
            }

        }

        public void Load()
        {
            TcpClient.Delimiter = 0x13;
            TcpClient.StringEncoder = Encoding.UTF8;
            
        }

        public void LoadCam()
        {
            TcpClientCamera.Delimiter = 0x13;
            TcpClientCamera.StringEncoder = Encoding.UTF8;

        }


    }
}



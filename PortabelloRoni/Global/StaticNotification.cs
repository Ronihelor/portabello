﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortabelloRoni.Global
{
    public class StaticNotiication
    {
        public static string Connected = "connected";
        public static string FailedConnection = "connection failed";
        public static string Error = "error";
        public static string SendError = "failed sending";
        public static string DownloadError = "Download Error";
        public static string IP = "127.0.0.1";
        public static string Port = "8910";
        public static string PortCamera = "21";
        public static int WidthVid = 250;
        public static int HeightVid = 200;

        #region Server FTP

        public static int PortFTPFile = 21;

        #endregion

    }
}

﻿using PortabelloRoni.BL;
using PortabelloRoni.GUI.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace PortabelloRoni.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for CamControll.xaml
    /// </summary>
    public partial class CamControll : UserControl
    {

        public CamControll()
        {
            InitializeComponent();
        }

        private void buttonSend_Click(object sender, RoutedEventArgs e)
        {
            Logic.Instance.TCPIPMan.Send();
        }
    }
}

﻿using PortabelloRoni.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PortabelloRoni.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for Connect.xaml
    /// </summary>
    public partial class Connect : UserControl
    {
        public delegate void OnTriggerDelegate(object sender, EventArgs e);

        public Connect()
        {
            InitializeComponent();
        }


        private void ButtonCameraConnect_Click(object sender, RoutedEventArgs e)
        {

            Logic.Instance.CameraConnect();
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            Logic.Instance.Connect();
        }
    }
}

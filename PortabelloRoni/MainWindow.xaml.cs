﻿using Newtonsoft.Json;
using PortabelloRoni.BL;
using PortabelloRoni.Global;
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using PortabelloRoni.GUI.UserControls;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace PortabelloRoni
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public object FTPSite { get; private set; }
        public TabItem _tabUserPage { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            BitmapImage bImage = new BitmapImage(new Uri(@"C:\Users\Administrator\Desktop\PortabelloGIT\PortabelloRoni\Assets\Images\stream-unavailable.png"));
            Logic.Instance.CamMan.onTriggerFunction = Client_DataRecieved;
            Logic.Instance.TCPIPMan.onTriggerFunction = Client_DataRecieved;
            Logic.Instance._ImageSource = bImage;
            this.DataContext = Logic.Instance;
            Logic.Instance.TCPIPMan.Load();
            Logic.Instance.CamMan.Load();

            Logic.Instance._Ip = StaticNotiication.IP;
            Logic.Instance._Port = StaticNotiication.Port;
            Logic.Instance._PortCamera = StaticNotiication.PortCamera;
            Logic.Instance._Width = StaticNotiication.WidthVid;
            Logic.Instance._Height = StaticNotiication.HeightVid;
            Logic.Instance._ProgressBarVisible = Visibility.Hidden;

        }


        private void ButtonFiles_Click(object sender, RoutedEventArgs e)
        {
            FTPClientFilesForm OpenWindow = new FTPClientFilesForm();
            OpenWindow.Show();
        }

     

        public void Client_DataRecieved(string e, bool IsCamera)
        {

           
            string replyMessage = e.Replace("\u0013", "");

            if (replyMessage == StaticNotiication.Connected || replyMessage == StaticNotiication.FailedConnection)
            {
                Logic.Instance._ErrorText = (replyMessage);
            }
            else
            {
                try
                {
                    string json = replyMessage;
                    if (IsCamera == true)
                    {
                        Dispatcher.BeginInvoke(new Action(delegate
                        {
                            Logic.Instance._ImageSource = Logic.Instance.ToImage(Convert.FromBase64String(json));
                        }));
                    }

                    else
                    {
                        JsonCreator newJson = JsonConvert.DeserializeObject<JsonCreator>(json);
                        Logic.Instance.JsonInstance = new JsonCreator(newJson);
                        Logic.Instance.Loadtointerface(newJson);
                    }

                }
                catch
                {

                    Logic.Instance._ErrorText = (StaticNotiication.Error);
                }
            }



        }
    }
}
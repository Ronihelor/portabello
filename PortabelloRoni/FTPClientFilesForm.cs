﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PortabelloRoni.BL;
using FileTransferProtocolLib;
using PortabelloRoni.Global;
using System.IO;
using System.Net.Http;
using System.Threading;

namespace PortabelloRoni
{
    public partial class FTPClientFilesForm : Form
    {


        FTPManagerClass manager;
    


        public FTPClientFilesForm()
        {
            InitializeComponent();
        }

        private void buttonGetFiles_Click(object sender, EventArgs e)
        {
            string[] files = manager.GetFilesOnServer("");
            treeView1.Nodes.Clear();
            foreach (string filename in files)
            {

               treeView1.Nodes.Add(filename);
            }

            


        }

        //public void addFilesOnServertoIF(TreeNode P, string path)
        //{

        //    if (P == null)
        //        return;
        //    try
        //    {
        //        string[] files = manager.GetFilesOnServer(path + "/" + P.Text);


        //        foreach (String filename in files)
        //        {

        //            P.Nodes.Add(filename);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
           
        //}


        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode CurrentNode = e.Node;
            string PathNode = CurrentNode.Text;
            if (CurrentNode == null)
                return;
            string[] files = manager.GetFilesOnServer(PathNode);

            if (files != null)
            {
                foreach (String filename in files)
                {

                    CurrentNode.Nodes.Add(filename);

                }
            }
            
        }




        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                manager = new FTPManagerClass(textBoxUserName.Text, textBoxPassword.Text, textBoxHost.Text, TextBoxPort.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }



        private void button6_Click(object sender, EventArgs e)
        {
            manager.CreateDir(textBox4.Text);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            manager.Delete(textBox11.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            manager.Rename(textBox10.Text, textBox9.Text);
        }



        private void buttonDownload_Click(object sender, EventArgs e)
        {

            manager.DownloadFile(textBoxDown.Text, textBox8.Text);
        }



        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            string ext = textBoxDown.Text.Substring(textBoxDown.Text.IndexOf("."));
            sfd.Filter = "Download file extension(" + ext + ")|*" + ext;
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox8.Text = sfd.FileName;
            }
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            string PathNode = treeView1.SelectedNode.Text;
            textBoxDown.Text = PathNode;

        }


       
    }
}

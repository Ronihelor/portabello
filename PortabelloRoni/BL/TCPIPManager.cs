﻿using PortabelloRoni.DAL;
using PortabelloRoni.Global;
using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace PortabelloRoni.BL
{
    public class TCPIPManager
    {
        private TCPIP Client { get; set; }
        public delegate void OnTriggerDelegate(string s, bool IsCamera);
        public OnTriggerDelegate onTriggerFunction;


        public TCPIPManager()
        {
            Client = new TCPIP();
        }
        
       

        private void Client_DataRecieved(object sender, Message e)
        {

            onTriggerFunction(e.MessageString, false);
        }


        public void Connect()
        {
            if (Client.Connect(Logic.Instance._Ip, Logic.Instance._Port))
            {
                Logic.Instance._IsConnected = false;
                Logic.Instance._ErrorText = (StaticNotiication.Connected);
            }
            else
            {
                Logic.Instance._ErrorText = (StaticNotiication.FailedConnection);
            }
        }

        public void Send()
        {
            if (Client.Send(Logic.Instance.JsonInstance.JsonCreatorReturn()))
            {
                Logic.Instance._ErrorText = (StaticNotiication.FailedConnection);
            }
        }

        public void Load()
        {

            Client.Load();
            Client.TcpClient.DataReceived += Client_DataRecieved;
        }
    }
}



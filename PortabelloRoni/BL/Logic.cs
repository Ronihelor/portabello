﻿using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Sockets;
using System.Collections.Concurrent;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using Newtonsoft.Json;
using System.Windows.Threading;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using Newtonsoft.Json.Linq;
using PortabelloRoni.DAL;
using PortabelloRoni.Global;

namespace PortabelloRoni.BL
{
    public class Logic : ObservableObject
    {

        #region DM
        protected BitmapImage ImageSource;
        protected double RecordingTime;
        protected bool? RecordingMode;
        protected bool? Detector;
        protected double FPS;
        protected string WifiName;
        protected string Password;
        protected double ParseScan;
        protected int Width;
        protected int Height;
        protected bool? CameraIsConnected;
        protected static bool? IsConnected;
        protected string Ip;
        protected string Port;
        protected string PortCamera;
        protected string ErrorText;
        protected Visibility ProgressBarVisible;
        protected int ProgressVal;

        #endregion

        #region Props
        public StaticNotiication StatNot;
        public CameraManager CamMan { get; set; }
        public TCPIPManager TCPIPMan { get; set; }
        #endregion

        public JsonCreator JsonInstance { get; set; }

        #region Instance
        private static Logic instance;
        public static Logic Instance { get { return instance ?? (instance = new Logic()); } }
        #endregion

        public Logic()
        {

            JsonInstance = new JsonCreator();
            TCPIPMan = new TCPIPManager();
            CamMan = new CameraManager();
            StatNot = new StaticNotiication();

        }

        public void Send()
        {
            TCPIPMan.Send();
        }

        public void Connect()
        {
            TCPIPMan.Connect();
        }

        public void CameraConnect()
        {
            CamMan.Connect();
        }

        public void Loadtointerface(JsonCreator newJson) //loads the cake given and changes interface according to what was given.
        {


            if (newJson.RecordingMode != null)
            {
                Logic.Instance._RecordingMode = newJson.RecordingMode.Value;
            }

            if (newJson.Detector != null)
            {
                Logic.Instance._Detector = newJson.Detector.Value;
            }

            if (newJson.RecordingTime != -1)
            {
                Logic.Instance._RecordingTime = newJson.RecordingTime;
            }

            if (newJson.FPS != -1)
            {
                Logic.Instance._FPS = newJson.FPS;
            }

            if (newJson.WifiName != null)
            {
                Logic.Instance._WifiName = newJson.WifiName;
            }

            if (newJson.Password != null)
            {
                Logic.Instance._Password = newJson.Password;
            }

            if (newJson.ParseScan != -1)
            {
                Logic.Instance._ParseScan = newJson.ParseScan;
            }


        }


        public BitmapImage ToImage(byte[] PicArray)
        {
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new System.IO.MemoryStream(PicArray);
            image.EndInit();
            return image;
        }

        #region Binding

        public BitmapImage _ImageSource
        {
            get { return ImageSource; }
            set
            {
                ImageSource = value;
                RaisePropertyChangedEvent("_ImageSource");
            }

        }

        public double _RecordingTime
        {
            get { return RecordingTime; }
            set
            {
                RecordingTime = value;
                RaisePropertyChangedEvent("_RecordingTime");
            }

        }


        public int _ProgressVal
        {
            get { return ProgressVal; }
            set
            {
                ProgressVal = value;
                RaisePropertyChangedEvent("_ProgressVal");
            }

        }

        public bool? _RecordingMode
        {
            get { return RecordingMode; }
            set
            {
                RecordingMode = value;
                RaisePropertyChangedEvent("_RecordingMode");
            }

        }

        public Visibility _ProgressBarVisible
        {
            get { return ProgressBarVisible; }
            set
            {
                ProgressBarVisible = value;
                RaisePropertyChangedEvent("_ProgressBarVisible");
            }

        }

        public string _Ip
        {
            get { return Ip; }
            set
            {
                Ip = value;
                RaisePropertyChangedEvent("_Ip");
            }

        }

        public string _Port
        {
            get { return Port; }
            set
            {
                Port = value;
                RaisePropertyChangedEvent("_Port");
            }

        }

        public string _PortCamera
        {
            get { return PortCamera; }
            set
            {
                PortCamera = value;
                RaisePropertyChangedEvent("_PortCamera");
            }

        }
        public bool? _Detector
        {
            get { return Detector; }
            set
            {
                Detector = value;
                RaisePropertyChangedEvent("_Detector");
            }
        }
        public double _FPS
        {
            get { return FPS; }
            set
            {
                FPS = value;
                RaisePropertyChangedEvent("_FPS");
            }

        }
        public string _WifiName
        {
            get { return WifiName; }
            set
            {
                WifiName = value;
                RaisePropertyChangedEvent("_WifiName");
            }

        }

        public string _Password
        {
            get { return Password; }
            set
            {
                Password = value;
                RaisePropertyChangedEvent("_Password");
            }

        }
        public double _ParseScan
        {
            get { return ParseScan; }
            set
            {
                ParseScan = value;
                RaisePropertyChangedEvent("_ParseScan");

            }
        }

        public int _Width
        {
            get { return Width; }
            set
            {
                Width = value;
                RaisePropertyChangedEvent("_Width");

            }
        }

        public int _Height
        {
            get { return Height; }
            set
            {
                Height = value;
                RaisePropertyChangedEvent("_Height");

            }
        }


        public bool? _IsConnected
        {
            get { return IsConnected; }
            set
            {
                IsConnected = value;
                RaisePropertyChangedEvent("_IsConnected");
            }
        }

        public bool? _CameraIsConnected
        {
            get { return CameraIsConnected; }
            set
            {
                CameraIsConnected = value;
                RaisePropertyChangedEvent("_CameraIsConnected");
            }
        }

        public string _ErrorText
        {
            get { return ErrorText; }
            set
            {
                ErrorText = value;
                RaisePropertyChangedEvent("_ErrorText");
            }
        }


        #endregion  



    }
}

﻿using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Sockets;
using System.Collections.Concurrent;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using Newtonsoft.Json;
using System.Windows.Threading;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using PortabelloRoni.DAL;
using PortabelloRoni.Global;

namespace PortabelloRoni.BL
{
    public class CameraManager
    {
        private TCPIP Client { get; set; }
        public delegate void OnTriggerDelegate(string s, bool IsCamera);
        public OnTriggerDelegate onTriggerFunction;

        public CameraManager()
        {
            Client = new TCPIP();
        }

       
        public void Connect()
        {
            if (Client.ConnectCamera(Logic.Instance._Ip, Logic.Instance._PortCamera))
            {
                Logic.Instance._ErrorText = (StaticNotiication.Connected);
                Logic.Instance._CameraIsConnected = false;
            }

            else
            {
                Logic.Instance._ErrorText = (StaticNotiication.FailedConnection);
            }
        }

   
        public void Load()
        {
            Client.LoadCam();
            Client.TcpClientCamera.DataReceived += Client_DataRecieved;
        }

        public void Client_DataRecieved(object sender, Message e)
        {

            onTriggerFunction(e.MessageString, true);
        }



    }


}

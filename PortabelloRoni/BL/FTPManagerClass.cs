﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using PortabelloRoni.Global;
using System.Threading;
using System.Windows;
using PortabelloRoni.BL;

namespace PortabelloRoni
{
    public class FTPManagerClass
    {
        private static string password = "";
        private static string username = "";
        private static string host = "";
        private static string port = "";

        private FtpWebRequest ftpRequest = null;
        private FtpWebResponse ftpResponse = null;
        private Stream ftpStream = null;


        public FTPManagerClass(string user, string pass, string hostname,string _port)
        {
            username = user;
            password = pass;
            host = hostname;
            port = _port;
        }


        //public void DownloadFile(string file, string localDestnDir)
        //{
        //    try
        //    {
        //        string uri = host + "/" + file;
        //        Uri serverUri = new Uri(uri);
        //        if (serverUri.Scheme != Uri.UriSchemeFtp)
        //        {
        //            return;
        //        }

        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://"+host + "/" + file);
        //        request.UseBinary = true;
        //        request.Method = WebRequestMethods.Ftp.DownloadFile;
        //        request.Credentials = new NetworkCredential(username, password);
        //        request.KeepAlive = false;
        //        request.UsePassive = false;
        //        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
        //        Stream responseStream = response.GetResponseStream();
        //        FileStream writeStream = new FileStream(localDestnDir + "\\" + file, FileMode.Create);
        //        int Length = 2048;
        //        Byte[] buffer = new Byte[Length];
        //        int bytesRead = responseStream.Read(buffer, 0, Length);
        //        while (bytesRead > 0)
        //        {
        //            writeStream.Write(buffer, 0, bytesRead);
        //            bytesRead = responseStream.Read(buffer, 0, Length);
        //        }
        //        writeStream.Close();
        //        response.Close();
        //    }
        //    catch (WebException wEx)
        //    {
        //        MessageBox.Show(wEx.Message, "Download Error");
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Download Error");
        //    }
        //}


        public void DownloadFile(string ServerFileName, string ComputerFileName)
        {
            try
            {
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://" + host + ":" + port + "/" + ServerFileName); //מבקשת תיקייה בשם מסויים 
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                ftpStream = ftpResponse.GetResponseStream();
                FileStream fs = new FileStream(ComputerFileName, FileMode.OpenOrCreate);
                byte[] byteBuffer = new byte[Convert.ToInt32(GetFileSize(ServerFileName))];
                int bytesRead = ftpStream.Read(byteBuffer, 0, Convert.ToInt32(GetFileSize(ServerFileName)));
                try
                {
                    Logic.Instance._ProgressBarVisible = Visibility.Visible;
                    MessageBox.Show("Downloading");
                    while (bytesRead > 0)
                    {
                        fs.Write(byteBuffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(byteBuffer, 0, Convert.ToInt32(GetFileSize(ServerFileName)));
                        Logic.Instance._ProgressVal = bytesRead;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                ftpResponse.Close();
                ftpStream.Close();
                fs.Close();
                ftpRequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            
        }



        public void Rename(string oldname, string newname)
        {
            try
            {
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://" + host + ":" + port + "/" + oldname);
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.Rename;
                ftpRequest.RenameTo = newname;
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                ftpResponse.Close();
                ftpRequest = null;
            }

            catch (Exception ex)

            {

                MessageBox.Show(ex.Message);

            }
        }
       
        public string[] GetFilesOnServer(string dir)
        {

            
            try
            {
                
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create("ftp://"+host+":"+port+"/"+dir);
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.UseBinary = true;
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                ftpRequest.Proxy = null;
                ftpRequest.KeepAlive = true;
                ftpRequest.UsePassive = true;
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                ftpStream = ftpResponse.GetResponseStream();
                StreamReader sr = new StreamReader(ftpStream);
                string dirRaw = null;
                try
                {
                    while (sr.Peek() != -1)
                    {
                        dirRaw += sr.ReadLine() + "|";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                ftpResponse.Close();
                sr.Close();
                ftpStream.Close();
                ftpRequest = null;
                try
                {
                    string [] filesInDir = new string[dirRaw.Split("|".ToCharArray()).Length];
                    filesInDir = dirRaw.Split("|".ToCharArray());
                    return filesInDir;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }
    

        public void Delete(string filename)
        {
            try
            {
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + filename);
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void CreateDir(string name)
        {
            try
            {
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + name);
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public long GetFileSize(string filename)
        {
            long size;

            FtpWebRequest sizeRequest = (FtpWebRequest)FtpWebRequest.Create(host + "/" + filename);
            sizeRequest.Credentials = new NetworkCredential(username, password);
            sizeRequest.Method = WebRequestMethods.Ftp.GetFileSize;
            sizeRequest.UseBinary = true; //העברה היא בינארית בניגוד למחרוזת


            FtpWebResponse respSize = (FtpWebResponse)sizeRequest.GetResponse();
            size = respSize.ContentLength;

            return size;
        }


    }
}



